﻿using ShopAPI.Models.Product;

namespace ShopAPI.Repos.ProductRepo;

public class ProductRepoImpl : IProductRepo
{
    private ShopDbContext _context;

    public ProductRepoImpl(ShopDbContext context)
    {
        _context = context;
    }

    public IList<Product> All()
    {
        return _context.Products.ToList();
    }

    public Product? Find(int id)
    {
        return _context.Products.Find(id);
    }

    public Product Create(string name, double price, int stock)
    {
        var product = new Product(name, price, stock);
        _context.Products.Add(product);
        _context.SaveChanges();
        return product;
    }

    public Product Update(Product product)
    {
        _context.Products.Update(product);
        _context.SaveChanges();
        return product;
    }

    public Product? Delete(int id)
    {
        Product? product = _context.Products.Find(id);
        if (product != null)
        {
            _context.Products.Remove(product);
            _context.SaveChanges();
        }
        return product;
    }
}
