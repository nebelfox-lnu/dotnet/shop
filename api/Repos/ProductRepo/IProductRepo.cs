﻿using ShopAPI.Models.Product;

namespace ShopAPI.Repos.ProductRepo;

public interface IProductRepo
{
    IList<Product> All();
    Product? Find(int id);
    Product Create(string name, double price, int stock);
    Product Update(Product product);
    Product? Delete(int id);
}
