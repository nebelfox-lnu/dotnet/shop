﻿using ShopAPI.Models.Order;

namespace ShopAPI.Repos.OrderRepo;

public interface IOrderRepo
{
    IList<Order> All();
    Order? Find(int id);
    Order Create(IList<OrderItem> items);
    Order Update(Order order);
    Order? Delete(int id);
}
