﻿using ShopAPI.Models.Order;
using ShopAPI.Models.Order;

namespace ShopAPI.Repos.OrderRepo;

public class OrderRepoImpl : IOrderRepo
{
    private ShopDbContext _context;

    public OrderRepoImpl(ShopDbContext context)
    {
        _context = context;
    }

    public IList<Order> All()
    {
        return _context.Orders.ToList();
    }

    public Order? Find(int id)
    {
        return _context.Orders.Find(id);
    }

    public Order Create(IList<OrderItem> items)
    {
        var order = new Order(items);
        _context.Orders.Add(order);
        _context.SaveChanges();
        return order;
    }

    public Order Update(Order order)
    {
        _context.Orders.Update(order);
        _context.SaveChanges();
        return order;
    }

    public Order? Delete(int id)
    {
        Order? order = _context.Orders.Find(id);
        if (order != null)
        {
            _context.Orders.Remove(order);
            _context.SaveChanges();
        }
        return order;
    }
}
