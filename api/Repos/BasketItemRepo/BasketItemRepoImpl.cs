﻿using ShopAPI.Models.Basket;

namespace ShopAPI.Repos.BasketItemRepo;

public class BasketItemRepoImpl : IBasketItemRepo
{
    private ShopDbContext _context;

    public BasketItemRepoImpl(ShopDbContext context)
    {
        _context = context;
    }

    public IList<BasketItem> All()
    {
        return _context.BasketItems.ToList();
    }

    public BasketItem? Find(int id)
    {
        return _context.BasketItems.Find(id);
    }

    public BasketItem Create(int productId, int quantity)
    {
        var item = new BasketItem(productId, quantity);
        _context.BasketItems.Add(item);
        _context.SaveChanges();
        return item;
    }

    public BasketItem Update(BasketItem item)
    {
        _context.BasketItems.Update(item);
        _context.SaveChanges();
        return item;
    }

    public BasketItem? Delete(int id)
    {
        BasketItem? item = _context.BasketItems.Find(id);
        if (item != null)
        {
            _context.BasketItems.Remove(item);
            _context.SaveChanges();
        }
        return item;
    }
}
