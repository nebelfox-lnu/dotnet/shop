﻿using ShopAPI.Models.Basket;

namespace ShopAPI.Repos.BasketItemRepo;

public interface IBasketItemRepo
{
    IList<BasketItem> All();
    BasketItem? Find(int id);
    BasketItem Create(int productId, int quantity);
    BasketItem Update(BasketItem item);
    BasketItem? Delete(int id);
}
