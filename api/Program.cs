using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ShopAPI;
using ShopAPI.Repos.BasketItemRepo;
using ShopAPI.Repos.OrderRepo;
using ShopAPI.Repos.ProductRepo;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<ShopDbContext>(
    o => o.UseNpgsql(builder.Configuration.GetConnectionString("ShopDB")));

builder.Services.AddScoped<IProductRepo, ProductRepoImpl>();
builder.Services.AddScoped<IOrderRepo, OrderRepoImpl>();
builder.Services.AddScoped<IBasketItemRepo, BasketItemRepoImpl>();

builder.Services.AddControllers();
builder.Services.AddRazorPages();
       // .AddRazorPagesOptions(options => options.Conventions.AddPageRoute("/Pages/Products", "Pages/Products"));
builder.Services.AddCors(o => o.AddPolicy("Policy",
                                          b =>
                                          {
                                              b.AllowAnyOrigin()
                                               .AllowAnyMethod()
                                               .AllowAnyHeader();
                                          }));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(
    options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()
);


app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapRazorPages();
});



app.MapControllers();

app.Run();
