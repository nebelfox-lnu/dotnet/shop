﻿using Microsoft.AspNetCore.Mvc;
using ShopAPI.Models.Product;
using ShopAPI.Repos.ProductRepo;

namespace ShopAPI.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepo _repo;

        public ProductsController(IProductRepo repo)
        {
            _repo = repo;
        }
        
        /// <summary>
        /// Get the list of all the products
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Product>), 200)]
        public IEnumerable<Product> Get()
        {
            return _repo.All();
        }

        /// <summary>
        /// Get a specific product by its identifier
        /// </summary>
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Product), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetById([FromRoute] int id)
        {
            Product? product = _repo.Find(id);
            return product is null ? NotFound() : Ok(product);
        }
        
        /// <summary>
        /// Add a new item to the list
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Product), 200)]
        [ProducesResponseType(404)]
        public IActionResult Create([FromBody] ProductProps props)
        {
            return Ok(_repo.Create(props.Name, props.Price, props.Stock));
        }
        
        [HttpPut]
        [ProducesResponseType(typeof(Product), 200)]
        [ProducesResponseType(404)]
        public IActionResult Update([FromBody] Product product)
        {
            return Ok(_repo.Update(product));
        }
        
        [HttpDelete("{id:int}")]
        [ProducesResponseType(typeof(Product), 200)]
        [ProducesResponseType(404)]
        public IActionResult Delete([FromRoute] int id)
        {
            Product? product = _repo.Delete(id);
            return product is null ? NotFound() : Ok(product);
        }
    }
}
