﻿using Microsoft.AspNetCore.Mvc;
using ShopAPI.Models.Order;
using ShopAPI.Repos.OrderRepo;
using ShopAPI.Views;

namespace ShopAPI.Controllers;

[ApiController]
[Route("orders")]
public class OrdersController : ControllerBase
{
    private readonly IOrderRepo _repo;

    public OrdersController(IOrderRepo repo)
    {
        _repo = repo;
    }
        
    /// <summary>
    /// Get the list of all the products
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Order>), 200)]
    public IEnumerable<Order> Get()
    {
        return _repo.All();
    }

    /// <summary>
    /// Get a specific product by its identifier
    /// </summary>
    [HttpGet("{id:int}")]
    [ProducesResponseType(typeof(OrderView), 200)]
    [ProducesResponseType(404)]
    public IActionResult GetById([FromRoute] int id)
    {
        Order? order = _repo.Find(id);
        return order is null 
            ? NotFound()
            : Ok(order);
    }
        
    /// <summary>
    /// Add a new item to the list
    /// </summary>
    [HttpPost]
    [ProducesResponseType(typeof(OrderView), 200)]
    [ProducesResponseType(404)]
    public IActionResult Create([FromBody] OrderProps props)
    {
        return Ok(_repo.Create(props.Items));
    }
}