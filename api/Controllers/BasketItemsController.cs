﻿using Microsoft.AspNetCore.Mvc;
using ShopAPI.Models.Basket;
using ShopAPI.Repos.BasketItemRepo;
using ShopAPI.Views;

namespace ShopAPI.Controllers;

[ApiController]
[Route("basket-items")]
public class BasketItemsController : ControllerBase
{
    private IBasketItemRepo _repo;

    public BasketItemsController(IBasketItemRepo repo)
    {
        _repo = repo;
    }

    /// <summary>
    /// Get the list of all the items in the basket
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<BasketItemView>), 200)]
    public IActionResult Get()
    {
        return Ok(_repo.All());
    }

    /// <summary>
    /// Get a specific item in the basket by its identifier
    /// </summary>
    [HttpGet("{id:int}")]
    [ProducesResponseType(typeof(BasketItem), 200)]
    [ProducesResponseType(404)]
    public IActionResult GetById([FromRoute] int id)
    {
        BasketItem? item = _repo.Find(id);
        return item is null
            ? NotFound() 
            : Ok(item);
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(BasketItemView), 200)]
    [ProducesResponseType(404)]
    public IActionResult Create([FromBody] BasketItemProps props)
    {
        return Ok(_repo.Create(props.ProductId, props.Quantity));
    }

    [HttpPut]
    [ProducesResponseType(typeof(BasketItemView), 200)]
    public IActionResult Update([FromBody] BasketItem item)
    {
        return Ok(_repo.Update(item));
    }

    [HttpDelete]
    [ProducesResponseType(typeof(BasketItemView), 200)]
    public IActionResult Delete([FromRoute] int id)
    {
        return Ok(_repo.Delete(id));
    }
}
