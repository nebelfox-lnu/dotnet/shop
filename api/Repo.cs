﻿using ShopAPI.Models;

namespace ShopAPI;

public class Repo<TItemProps, TItem> where TItem : Entity
{
    public Dictionary<int, TItem> Items { get; }
    private Func<int, TItemProps, TItem> _itemFactory;
    public int NextId { get; private set; }

    public Repo(Func<int, TItemProps, TItem> itemFactory)
    {
        _itemFactory = itemFactory;
        Items = new Dictionary<int, TItem>();
    }

    public TItem Create(TItemProps props)
    {
        int id = NextId++;
        TItem item = _itemFactory(id, props);
        Items.Add(id, item);
        return item;
    }

    public TItem Update(int id, TItemProps props)
    {
        if (!Items.ContainsKey(id))
            throw new KeyNotFoundException($"Item with id {id} doesn't exist");
        TItem item = _itemFactory(id, props);
        Items[id] = item;
        return item;
    }

    public bool Delete(int id)
    {
        return Items.Remove(id);
    }

    public void Clear()
    {
        Items.Clear();
    }
}
