﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShopAPI.Models.Product;

namespace ShopAPI.Pages;

public class ProductPage : PageModel
{
    private readonly ShopDbContext _context;

    public ProductPage(ShopDbContext context)
    {
        _context = context;
    }
    
    public Product? Product { get; set; }

    public IActionResult OnGet(int id)
    {
        // Fetch the product from the database using the provided ID
        // Populate the ProductModel with the retrieved product details
        // For simplicity, let's assume you have a ProductService to handle database operations

        Product = _context.Products.Find(id);

        // if (product == null)
        // {
        //     return NotFound();
        // }

        // Populate the ProductModel with the retrieved product details
        // Product = product;

        return Page();
    }
}
