﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShopAPI.Models.Product;

namespace ShopAPI.Pages;

public class ProductsPage : PageModel
{
    private readonly ShopDbContext _context;

    public ProductsPage(ShopDbContext context)
    {
        _context = context;
    }

    public List<Product> ProductsList { get; set; }

    public IActionResult OnGet()
    {
        ProductsList = _context.Products.ToList();
        return Page();
    }
}
