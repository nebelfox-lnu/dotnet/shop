﻿using ShopAPI.Models;
using ShopAPI.Models.Product;

namespace ShopAPI.Views;

public record BasketItemView(int Id, int Quantity, Product Product) : Entity;
