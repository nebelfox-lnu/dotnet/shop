﻿using ShopAPI.Models;
using ShopAPI.Models.Order;

namespace ShopAPI.Views;

public record OrderView(int Id, IEnumerable<OrderItemView> Items) : Entity;
