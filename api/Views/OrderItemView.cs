﻿using ShopAPI.Models;
using ShopAPI.Models.Order;
using ShopAPI.Models.Product;

namespace ShopAPI.Views;

public class OrderItemView : OrderItemBase, Entity
{
    public int Id { get; }
    
    public Product Product { get; }
    
    public OrderItemView(int id, int orderId, int quantity, Product product) : base(orderId, quantity)
    {
        Id = id;
        Product = product;
    }
}
