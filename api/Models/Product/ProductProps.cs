﻿using System.ComponentModel.DataAnnotations;

namespace ShopAPI.Models.Product;

public class ProductProps
{
    [Required]
    public string Name { get; set; }
    [Required]
    public double Price { get; set; }
    [Required]
    public int Stock { get; set; }

    public ProductProps(string name, double price, int stock)
    {
        Name = name;
        Price = price;
        Stock = stock;
    }
}
