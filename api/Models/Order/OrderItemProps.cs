﻿namespace ShopAPI.Models.Order;

public class OrderItemProps : OrderItemBase
{
    public int ProductId { get; }

    public OrderItemProps(int orderId, int productId, int quantity) : base(orderId, quantity)
    {
        ProductId = productId;
    }
}
