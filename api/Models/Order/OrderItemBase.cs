﻿namespace ShopAPI.Models.Order;

public class OrderItemBase
{
    public int OrderId { get; }
    public int Quantity { get; set; }
    
    public OrderItemBase(int orderId, int quantity)
    {
        OrderId = orderId;
        Quantity = quantity;
    }
}
