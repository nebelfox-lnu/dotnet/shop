﻿namespace ShopAPI.Models.Order;

public class OrderProps
{
    public IList<OrderItem> Items { get; }
    
    public OrderProps(IList<OrderItem> items)
    {
        Items = items;
    }
}
