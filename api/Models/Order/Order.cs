﻿namespace ShopAPI.Models.Order;

public class Order
{
    public int Id { get; set; }
    public IList<OrderItem> Items { get; set; }

    public Order(IList<OrderItem> items)
    {
        Items = items;
    }

    public Order()
    {
        Items = new List<OrderItem>();
    }
}
