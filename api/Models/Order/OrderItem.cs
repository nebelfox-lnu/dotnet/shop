﻿namespace ShopAPI.Models.Order;

public class OrderItem
{
    public int Id { get; set; }
    public Order Order { get; set; }
    public Product.Product Product { get; set; }
    public int Quantity { get; set; }

    public OrderItem(Order order, Product.Product product, int quantity)
    {
        Order = order;
        Product = product;
        Quantity = quantity;
    }

    public OrderItem()
    { }
}
