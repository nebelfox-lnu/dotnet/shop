﻿namespace ShopAPI.Models;

public interface Entity
{
    public int Id { get; }
}
