﻿namespace ShopAPI.Models.Basket;

public class BasketItemProps
{
    public int ProductId { get; }
    public int Quantity { get; set; }

    
    public BasketItemProps(int productId, int quantity)
    {
        ProductId = productId;
        Quantity = quantity;
    }
}
