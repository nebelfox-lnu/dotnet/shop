﻿namespace ShopAPI.Models.Basket;

public class BasketItem : Entity
{
    public int Id { get; set;  }
    public int ProductId { get; set; }
    public int Quantity { get; set; }

    public BasketItem(int id, BasketItemProps props)
    {
        Id = id;
        ProductId = props.ProductId;
        Quantity = props.Quantity;
    }

    public BasketItem(int productId, int quantity)
    {
        ProductId = productId;
        Quantity = quantity;
    }

    public BasketItem(int id, int productId, int quantity)
    {
        Id = id;
        ProductId = productId;
        Quantity = quantity;
    }
}
