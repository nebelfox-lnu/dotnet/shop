﻿namespace ShopAPI.Models.Basket;

public class Basket
{
    public IList<BasketItem> Items { get; }

    public Basket() : this(new List<BasketItem>())
    { }

    public Basket(IList<BasketItem> items)
    {
        Items = items;
    }
}
