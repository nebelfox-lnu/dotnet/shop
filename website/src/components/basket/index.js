import React, {useState} from "react";
import api from "../../api";
import '../items-list.css'

const BasketItem = ({basketItem, editCallback, deleteCallback}) => {
  const [isEditMode, setEditMode] = useState(false);
  const [quantity, setQuantity] = useState(basketItem.quantity);

  const onEdit = e => {
    setEditMode(false);
    editCallback(basketItem.id, {productId: basketItem.productId, quantity});
  }

  return isEditMode ? (
    <tr>
      <td>{basketItem.product.name}</td>
      <td>{basketItem.product.price}</td>
      <td><input type='number' name='stock' placeholder='Stock' defaultValue={basketItem.quantity}
                 onChange={e => setQuantity(Number(e.target.value))}/></td>
      <td>{quantity * basketItem.product.price}</td>
      <td className={'action-green'} onClick={onEdit}>Submit</td>
      <td className={'action-red'} onClick={() => setEditMode(false)}>Cancel</td>
    </tr>
  ) : (
    <tr key={basketItem.id}>
      <td>{basketItem.product.name}</td>
      <td>{basketItem.product.price}</td>
      <td>{basketItem.quantity}</td>
      <td>{basketItem.quantity * basketItem.product.price}</td>
      <td className={'action-yellow'} onClick={() => setEditMode(true)}>Edit</td>
      <td className={'action-red'} onClick={() => deleteCallback(basketItem.id)}>Delete</td>
    </tr>
  )
}

const Basket = () => {
  const [items, setItems] = useState(undefined);
  const [error, setError] = useState(undefined);

  items !== undefined || error || api.basket.getAll()
    .then(data => setItems(data))
    .catch(e => setError(e.message));

  const editCallback = (id, item) => {
    api.basket.update(id, item).then(r => setItems(undefined));
  };

  const deleteCallback = id => api.basket.delete(id).then(r => setItems(undefined));

  return (
    <div className={'items-list'}>
      {(items && items.length > 0) ? <table>
        <thead>
        <tr>
          <th>Product Name</th>
          <th>Product Price</th>
          <th>Quantity</th>
          <th>Total Price</th>
          <th colSpan={2}>Actions</th>
        </tr>
        </thead>
        <tbody>
        {(items || []).map(i => <BasketItem basketItem={i} editCallback={editCallback}
                                            deleteCallback={deleteCallback}/>)}
        <tr>
          <td className='action-green' colSpan={6} onClick={() => null}>Order</td>
        </tr>
        </tbody>
      </table> : <p>The basket is empty</p>}
    </div>
  )
};

export default Basket;
