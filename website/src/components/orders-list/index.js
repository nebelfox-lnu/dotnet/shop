import React, {useState} from "react";
import api from "../../api";
import '../items-list.css'

const Order = ({order}) => {
  return (
    <tr>
      <td>#{order.id}</td>
      <td>{order.items.length}</td>
      <td>{order.items.reduce((s, i) => s + i.quantity)}</td>
      <td>{order.items.reduce((s, i) => s + i.quantity * i.price)}</td>
    </tr>
  )
}

const OrdersList = () => {
  const [orders, setOrders] = useState(undefined);
  const [error, setError] = useState(undefined);

  orders !== undefined || error || api.orders.getAll()
    .then(data => setOrders(data))
    .catch(e => setError(e.message));

  return (
    <div className={'items-list'}>
      {(orders && orders.length > 0) ? <table>
        <thead>
        <tr>
          <th>Order ID</th>
          <th>Items</th>
          <th>Total Quantity</th>
          <th>Total Price</th>
        </tr>
        </thead>
        <tbody>
        {(orders || []).map(o => <Order order={o}/>)}
        </tbody>
      </table> : <p>There is no orders yet</p>}
    </div>
  )
};

export default OrdersList;
