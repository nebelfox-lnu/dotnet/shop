import React, {useState} from 'react';
import api from '../../api';
import '../items-list.css';


const VIEW_MODE = 0;
const ORDER_MODE = 1;
const EDIT_MODE = 2;

const Product = ({product, addToBasketCallback, editCallback, deleteCallback}) => {
  const [mode, setMode] = useState(VIEW_MODE);
  const [quantity, setQuantity] = useState(1);
  const [name, setName] = useState(product.name);
  const [price, setPrice] = useState(product.price);
  const [stock, setStock] = useState(product.stock);

  const onEdit = e => {
    setMode(VIEW_MODE);
    editCallback(product.id, {name, price, stock});
  }

  const onOrder = e => {
    setMode(VIEW_MODE);
    addToBasketCallback(product.id, quantity);
  }


  return mode === EDIT_MODE ? (
    <tr>
      <td><input name='name' placeholder='Name' defaultValue={product.name} onChange={e => setName(e.target.value)}/>
      </td>
      <td><input type='number' name='price' placeholder='Price' defaultValue={product.price}
                 onChange={e => setPrice(Number(e.target.value))}/></td>
      <td><input type='number' name='stock' placeholder='Stock' defaultValue={product.stock}
                 onChange={e => setStock(Number(e.target.value))}/></td>
      <td colSpan={2} className={'action-green'} onClick={onEdit}>Save</td>
      <td className={'action-red'} onClick={() => setMode(VIEW_MODE)}>Cancel</td>
    </tr>
  ) : (
    <tr key={product.id}>
      <td>{product.name}</td>
      <td>{product.price}</td>
      <td>{product.stock}</td>
      {mode === VIEW_MODE ? <>
        <td className={'action-green'} onClick={() => setMode(ORDER_MODE)}>Buy</td>
        <td className={'action-yellow'} onClick={() => setMode(EDIT_MODE)}>Edit</td>
        <td className={'action-red'} onClick={() => deleteCallback(product.id)}>Delete</td>
      </> : <>
        <td>
          <input type='number' name='quantity' placeholder='Quantity' defaultValue={quantity}
                 onChange={e => setQuantity(Number(e.target.value))}/>
        </td>
        <td className={'action-green'} onClick={onOrder}>Add</td>
        <td className={'action-red'} onClick={() => setMode(VIEW_MODE)}>Cancel</td>
      </>}

    </tr>
  )
}

const AddProduct = ({callback}) => {
  const [name, setName] = useState(undefined);
  const [price, setPrice] = useState(undefined);
  const [stock, setStock] = useState(undefined);

  return (
    <tr key='add-product'>
      <td><input name='name' placeholder='Name' onChange={e => setName(e.target.value)}/></td>
      <td><input type='number' name='price' placeholder='Price'
                 onChange={e => setPrice(Number(e.target.value))}/></td>
      <td><input type='number' name='stock' placeholder='Stock'
                 onChange={e => setStock(Number(e.target.value))}/></td>
      <td className={'action-green'} colSpan={3} onClick={() => callback({name, price, stock})}>Add</td>
    </tr>
  );
}

const ProductsList = () => {
  const [products, setProducts] = useState(undefined);
  const [error, setError] = useState(undefined);

  products !== undefined || error || api.products.getAll()
    .then(data => setProducts(data))
    .catch(e => setError(e.message));

  const addCallback = product => {
    api.products.create(product).then(r => setProducts(undefined));
  }

  const addToBasketCallback = (productId, quantity) => {
    api.basket.create({productId, quantity})
  }

  const editCallback = (id, product) => {
    api.products.update(id, product).then(r => setProducts(undefined));
  };

  const deleteCallback = id => api.products.delete(id).then(r => setProducts(undefined));

  return (
    <div className={'items-list'}>
      <table>
        <thead>
        <tr>
          {/*<th colSpan={2}>Add to basket</th>*/}
          <th>Product Name</th>
          <th>Price</th>
          <th>Available in stock</th>
          <th colSpan={3}>Actions</th>
        </tr>
        </thead>
        <tbody>
        {(products || []).map(p => <Product product={p} editCallback={editCallback} deleteCallback={deleteCallback} addToBasketCallback={addToBasketCallback}/>)}
        <AddProduct callback={addCallback}/>
        </tbody>
      </table>
    </div>
  )
};

export default ProductsList;
