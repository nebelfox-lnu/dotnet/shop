import {Link} from "react-router-dom";
import './style.css'

const Header = () => {
  return (
    <header className='header'>
      <h1>Shop</h1>
      <Link to='/products'><p>Browse Products Catalog</p></Link>
      <div className='align-right'>
        <Link to='/basket'><p>Basket</p></Link>
        <Link to='/orders'><p>Orders</p></Link>
      </div>
    </header>
  );
};

export default Header;
