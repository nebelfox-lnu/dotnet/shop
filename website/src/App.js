import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import './App.css';
import ProductsList from "./components/products-list";
import OrdersList from "./components/orders-list";
import Basket from "./components/basket";
import Header from "./components/header";

function App() {
  return (
    <BrowserRouter>
      <Header/>
      <Routes>
        <Route path='/products' element={<ProductsList/>}/>
        <Route path='/orders' element={<OrdersList/>}/>
        <Route path='/basket' element={<Basket/>}/>
        <Route path='/' element={<Navigate to='/products'/>}/>
        <Route path='*' element={<h1>Not Found</h1>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
