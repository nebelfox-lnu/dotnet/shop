import axios from 'axios'
import makeCrudAPI from "./utils";

const apiURL = process.env.API_URL || 'https://localhost:7236/'

const ax = axios.create({
  baseURL: apiURL
});

export const products = makeCrudAPI(ax, 'products');
export const orders = makeCrudAPI(ax, 'orders');
export const basketItems = makeCrudAPI(ax, 'basket-items');

const api = {
  products,
  orders,
  basket: basketItems
};
export default api;
