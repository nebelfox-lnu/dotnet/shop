const makers = {
  getAll: (axiosInstance, route) => () => axiosInstance.get(route).then(r => r.data),
  getById: (axiosInstance, route) => id => axiosInstance.get(`${route}/${id}`).then(r => r.data),
  create: (axiosInstance, route) => props => axiosInstance.post(route, props).then(r => r.data),
  update: (axiosInstance, route) => (id, props) => axiosInstance.put(`${route}/${id}`, props).then(r => r.data),
  delete: (axiosInstance, route) => id => axiosInstance.delete(`${route}/${id}`).then(r => r.data)
}

const makeCrudAPI = (axiosInstance, route, operations) => Object.fromEntries(
  (Object.keys(makers)).map(op => [op, makers[op](axiosInstance, route)]));
export default makeCrudAPI;
